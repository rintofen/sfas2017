﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Arena : MonoBehaviour
{
    //This camera is the game camera, not the UI camera.
    [SerializeField]
    private Camera Cam;

    //Whilst the Arena cube doesn't exist any more, we can still make use of these pre-calculated width and height variables to help us with knowing if objects have fallen off the screen or not.
    public static float Width { get; private set; }
    public static float Height { get; private set; }

    void Update()
    {
#if UNITY_EDITOR 
        if (!Application.isPlaying)
        {
            Calculate();
        }
#endif
    }

    //Calculates the width and height of the arena that exactly fills the camera's view.
    public void Calculate()
    {
        if (Cam != null)
        {
            Height = CameraUtils.FrustumHeightAtDistance(Cam.farClipPlane - 1.0f, Cam.fieldOfView);
            Width = Height * Cam.aspect;
            transform.localScale = new Vector3(Width * 0.1f, 1.0f, Height * 0.1f);
        }
    }
}
