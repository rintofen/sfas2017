﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour {

    //The fireball will rotate whilst it flies, SpinSpeed many degrees per second.
    [SerializeField]
    private float SpinSpeed;

    //Number of units per second that the fireball flies horizontally.
    [SerializeField]
    private float LatSpeed;

    //How much the fireball hurts enemies it collides with.
    [SerializeField]
    private int Damage;

    void Update()
    {
        //Only active if the game is playing, and if the game ends - destroy this.
        if (GameManager.GameState == GameManager.State.Playing)
        {
            transform.Rotate(Vector3.forward, SpinSpeed * Time.deltaTime);
            transform.position += Vector3.right * LatSpeed * Time.deltaTime;
        }
        else if (GameManager.GameState == GameManager.State.Menu)
        {
            Destroy(gameObject);
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Only hurt enemies, and destroy the fireball (1-time use!)
        if(collision.gameObject.tag=="Enemy")
        {
            collision.gameObject.SendMessage("Hurt", Damage);
            Destroy(gameObject);
        }
    }
}
