﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spook1 : MonoBehaviour {

    //Since enemies are 1-time, we can roll starting health and current health into the same variable:
    [SerializeField]
    private int Health;

    //Prefab for bullets
    [SerializeField]
    Shadowball1 shadowBallPrefab;

    //Time between shots
    [SerializeField]
    private float ShootCooldown;

    //Variables used for keeping track of spawning the enemy
    private bool mSpawning;
    private float mLerpVal;
    //The direction in which it will move.
    private Vector3 mDir;
    //Timer between bullet shots.
    private float mCooldown;
    //We need the player object in order to know where to aim. It's set by the GameManager when the enemy is spawned.
    private Player mPlayer;
    
    void Start () {
        mSpawning = true;
        mLerpVal = 0;
        mCooldown = 0;

        //Randomly choose between up and down for the Spook1 to move.
        int tDir = Random.Range(-1, 1);
        if (tDir == 0)
            tDir++;
        mDir = new Vector3(0, tDir, 0);
	}
	
	void Update () {
        if (GameManager.GameState == GameManager.State.Playing)
        {
            if (mSpawning)
            {
                //Linear interpolation that fades the enemy in.
                mLerpVal += Time.deltaTime;
                if (mLerpVal > 1)
                {
                    mSpawning = false;
                    GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r, GetComponent<SpriteRenderer>().color.g, GetComponent<SpriteRenderer>().color.b, 1);
                }
                else
                {
                    GetComponent<SpriteRenderer>().color += new Color(0, 0, 0, Time.deltaTime);
                }
            }
            else
            {
                transform.position += mDir * Time.deltaTime;
                mCooldown -= Time.deltaTime;
                if (mCooldown < 0)
                {
                    Shadowball1 shot = Instantiate(shadowBallPrefab);
                    shot.transform.position = transform.position;
                    shot.transform.parent = transform.parent;
                    
                    //Shoot directly towards the player
                    shot.SetDir(mPlayer.transform.position - transform.position);
                    mCooldown = ShootCooldown;
                }
            }
        }
	}

    //If hit by fireball, this is called
    public void Hurt(int damage)
    {
        if (!mSpawning)
        {
            Health -= damage;
            if (Health <= 0)
            {
                GameManager.EnemyKillEvent();
                Destroy(gameObject);
            }
        }
    }

    public void SetPlayer(Player p)
    {
        mPlayer = p;
    }
}
