﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadowball1 : MonoBehaviour {

    //The shadowball will rotate whilst it flies, SpinSpeed many degrees per second.
    [SerializeField]
    private float SpinSpeed;

    //Number of units per second that the fireball flies in direction of mDir
    [SerializeField]
    private float LatSpeed;

    //How much the shadowball hurts the player if it collides
    [SerializeField]
    private int Damage;

    //The direction that the shadowball will fly in (straight line)
    private Vector3 mDir;
	
	void Update ()
    {
        //Only active if the game is playing, and if the game ends - destroy this.
        if (GameManager.GameState == GameManager.State.Playing)
        {
            transform.Rotate(Vector3.forward, SpinSpeed * Time.deltaTime);
            transform.position += mDir * LatSpeed * Time.deltaTime;
        } else if(GameManager.GameState == GameManager.State.Menu)
        {
            Destroy(gameObject);
        }
	}

    //Simple setter for the direction of the shadowball
    public void SetDir(Vector3 dir)
    {
        mDir = dir.normalized;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Only hurt the player, and destroy the ball.
        if(collision.gameObject.tag=="Player")
        {
            collision.gameObject.SendMessage("Hurt", Damage);
            Destroy(gameObject);
        }
    }
}
