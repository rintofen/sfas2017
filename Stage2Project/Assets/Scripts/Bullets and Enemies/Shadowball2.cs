﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadowball2 : MonoBehaviour {

    //The shadowball will rotate whilst it flies, SpinSpeed many degrees per second.
    [SerializeField]
    private float SpinSpeed;

    //Number of units per second that the fireball flies in direction of mDir
    [SerializeField]
    private float LatSpeed;

    //How much the shadowball hurts the player if it collides
    [SerializeField]
    private int Damage;

    //the shadowball will act in a "2 balls on a chain" fashion at this speed
    [SerializeField]
    private float SineSpeed;

    //Used for flying in a sine wave
    private float mPos;
    private Vector3 mStartingPos;
    private Vector3 mDir;

    //Which type is this Shadowball2?
    private int mType;

    private void Start()
    {
        mStartingPos = transform.position;
        mPos = 0;
    }

    void Update ()
    {
        //Only active if the game is playing, and if the game ends - destroy this.
        if (GameManager.GameState == GameManager.State.Playing)
        {
            mPos += Time.deltaTime;
            if (mType == 1)
            {
                transform.Rotate(Vector3.forward, SpinSpeed * Time.deltaTime);
                transform.position = mStartingPos + mPos * mDir * LatSpeed + new Vector3(mDir.y * Mathf.Sin(Mathf.Deg2Rad * mPos * SineSpeed), mDir.x * Mathf.Sin(Mathf.Deg2Rad * mPos * SineSpeed), 0);
            }
            else
            {
                transform.Rotate(Vector3.forward, -SpinSpeed * Time.deltaTime);
                transform.position = mStartingPos + mPos * mDir * LatSpeed + new Vector3(mDir.y * -Mathf.Sin(Mathf.Deg2Rad * mPos * SineSpeed), mDir.x * -Mathf.Sin(Mathf.Deg2Rad * mPos * SineSpeed), 0);
            }

        } else if(GameManager.GameState == GameManager.State.Menu)
        {
            Destroy(gameObject);
        }
	}

    //Simple setter for the direction of the shadowball
    public void SetDir(Vector3 dir)
    {
        mDir = dir.normalized;
    }

    //type 1 is positive, everything else negative
    public void SetType(int t)
    {
        mType = t;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Only hurt the player, and destroy the ball.
        if(collision.gameObject.tag=="Player")
        {
            collision.gameObject.SendMessage("Hurt", Damage);
            Destroy(gameObject);
        }
    }
}
