﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spook2 : MonoBehaviour {

    //Since enemies are 1-time, we can roll starting health and current health into the same variable:
    [SerializeField]
    private int Health;

    //Prefab for bullets
    [SerializeField]
    Shadowball2 shadowBallPrefab;

    //Time between shots
    [SerializeField]
    private float ShootCooldown;

    //Variables used for keeping track of spawning the enemy
    private bool mSpawning;
    private float mLerpVal;
    //Spook2 moves in a sine wave towards the left, track how far along it is with mPos.
    private float mPos;
    private Vector3 mStartPos;
    //Timer between bullet shots.
    private float mCooldown;
    //We need the player object in order to know where to aim. It's set by the GameManager when the enemy is spawned.
    private Player mPlayer;
    
    void Start () {
        mSpawning = true;
        mLerpVal = 0;
        mCooldown = 0;
        mPos = 0;
        mStartPos = transform.position;
	}
	
	void Update () {
        if (GameManager.GameState == GameManager.State.Playing)
        {
            if (mSpawning)
            {
                //Linear interpolation that fades the enemy in.
                mLerpVal += Time.deltaTime;
                if (mLerpVal > 1)
                {
                    mSpawning = false;
                    GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r, GetComponent<SpriteRenderer>().color.g, GetComponent<SpriteRenderer>().color.b, 1);
                }
                else
                {
                    GetComponent<SpriteRenderer>().color += new Color(0, 0, 0, Time.deltaTime);
                }
            }
            else
            {
                mPos += Time.deltaTime;
                transform.position = mStartPos + new Vector3(-mPos*4,Mathf.Sin(Mathf.Deg2Rad*mPos*180),0);
                mCooldown -= Time.deltaTime;
                if (mCooldown < 0)
                {
                    Shadowball2 shot1 = Instantiate(shadowBallPrefab);
                    Shadowball2 shot2 = Instantiate(shadowBallPrefab);

                    shot1.transform.position = transform.position;
                    shot2.transform.position = transform.position;

                    shot1.SetDir(mPlayer.transform.position - transform.position);
                    shot2.SetDir(mPlayer.transform.position - transform.position);
                    
                    shot1.SetType(1);
                    shot2.SetType(2);

                    shot1.transform.parent = transform.parent;
                    shot2.transform.parent = transform.parent;

                    mCooldown = ShootCooldown;
                }
            }
        }
	}

    //If hit by fireball, this is called
    public void Hurt(int damage)
    {
        if (!mSpawning)
        {
            Health -= damage;
            if (Health <= 0)
            {
                GameManager.EnemyKillEvent();
                Destroy(gameObject);
            }
        }
    }

    public void SetPlayer(Player p)
    {
        mPlayer = p;
    }
}
