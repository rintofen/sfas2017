﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextShowingHighScore : MonoBehaviour {
    
	void Start ()
    {
        GetComponent<Text>().text = "High Score: " + SaveDataManager.CurrentUserData.mHighScore.ToString("0.00");
    }
}
