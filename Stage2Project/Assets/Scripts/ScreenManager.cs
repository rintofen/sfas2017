﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{
    //Events used throughout the game.
    public delegate void GameEvent();
    public static event GameEvent OnNewGame;
    public static event GameEvent OnEndGame;
    //The player class cannot call OnEndGame directly when it runs out of health, so it calls a static function here that executes an event chain, OnTEndGame is part of this.
    private static event GameEvent OnTEndGame;
    public static event GameEvent OnPause;
    public static event GameEvent OnUnpause;
    //A couple of events called whenever the menus move from the main menu to the customise menu or vice versa.
    public static event GameEvent OnMoveToCustomise;
    public static event GameEvent OnMoveToMainMenu;
    //Called whenever the wizard's clothing changes
    public static event GameEvent OnPlayerChange;

    //Our list of menu screens (also canvas object names)
    public enum Screens { TitleScreen, GameScreen, ResultScreen, CustomisationScreen, PauseScreen, NumScreens }

    //To help with transitioning (i.e. moving the UI camera)
    public enum TransitionState { None, Down, UpToTitle, UpToGame, Right, Left }
    
    private Canvas[] mScreens;
    private Screens mCurrentScreen;
    private Screens mPrevScreen;
    private TransitionState mTransState;
    private float mTransPos;

    void Awake()
    {
        //Copy the canvases into the array in the order they appear in the Screens enum
        mScreens = new Canvas[(int)Screens.NumScreens];
        Canvas[] screens = GetComponentsInChildren<Canvas>();
        for (int count = 0; count < screens.Length; ++count)
        {
            for (int slot = 0; slot < mScreens.Length; ++slot)
            {
                if (mScreens[slot] == null && ((Screens)slot).ToString() == screens[count].name)
                {
                    mScreens[slot] = screens[count];
                    break;
                }
            }
        }

        //Disable all but the title screen canvas.
        for (int screen = 1; screen < mScreens.Length; ++screen)
        {
            mScreens[screen].enabled = false;
        }

        mCurrentScreen = Screens.TitleScreen;
        mTransState = TransitionState.None;
        //The title screen is at an angle, apply now.
        transform.rotation = Quaternion.Euler(0, 20, 0);
        mTransPos = 1;

        //The only event called within ScreenManager itself, part of an external end game event chain.
        OnTEndGame += EndGame;
    }

    void Update()
    {
        //Move the screen with a spherical interpolation function
        if (TransitionState.Down == mTransState)
        {
            SmoothRotate(Quaternion.Euler(0, 20, 0), Quaternion.Euler(-90, 0, 0));
        } else if (TransitionState.UpToTitle == mTransState)
        {
            SmoothRotate(Quaternion.Euler(-90, 0, 0), Quaternion.Euler(0, 20, 0));
        }
        else if (TransitionState.UpToGame == mTransState)
        {
            SmoothRotate(Quaternion.Euler(-90, 0, 0), Quaternion.Euler(0, 0, 0));
        }
        else if (TransitionState.Right == mTransState)
        {
            SmoothRotate(Quaternion.Euler(0, 20, 0), Quaternion.Euler(0, -20, 0));
        } else if (TransitionState.Left == mTransState)
        {
            SmoothRotate(Quaternion.Euler(0, -20, 0), Quaternion.Euler(0, 20, 0));
        } else if(mCurrentScreen == Screens.ResultScreen)
        {
            transform.rotation = Quaternion.Euler(0, 20, 0);
        }
    }

    //"Smooth" because quadratically approaches the target rather than linearly
    private void SmoothRotate(Quaternion from, Quaternion to)
    {
        if (mTransPos > 0)
        {
            mTransPos -= Time.deltaTime * 2;
            this.transform.rotation = Quaternion.Slerp(from, to, 1.0f - Mathf.Pow(mTransPos, 2));
        }
        else
        {
            this.transform.rotation = to;
            mTransState = TransitionState.None;
            mScreens[(int)mPrevScreen].enabled = false;
        }
    }

    #region button transitions
    public void StartGame()
    {
        if (OnNewGame != null)
        {
            OnNewGame();
        }

        TransitionTo(Screens.GameScreen);
    }

    public void EndGame()
    {
        if(mCurrentScreen==Screens.PauseScreen)
        {
            if (OnUnpause != null)
            {
                OnUnpause();
            }
        }
        if (OnEndGame != null)
        {
            OnEndGame();
        }
        TransitionTo(Screens.ResultScreen);
    }

    public static void ExtEndGame()
    {
        if (OnTEndGame != null)
        {
            OnTEndGame();
        }
    }

    public void GoToTitle()
    {
        if (OnMoveToMainMenu != null)
        {
            OnMoveToMainMenu();
        }
        if(mCurrentScreen==Screens.PauseScreen)
        {
            if (OnUnpause != null)
            {
                OnUnpause();
            }
        }
        SaveDataManager.SaveUserData(); //The player probably came from customisation, so save their choices
        TransitionTo(Screens.TitleScreen);
    }

    public void GoToCustomise()
    {
        if (OnMoveToCustomise != null)
        {
            OnMoveToCustomise();
        }
        TransitionTo(Screens.CustomisationScreen);
    }

    public void GoToPause()
    {
        if (OnPause != null)
        {
            OnPause();
        }
        TransitionTo(Screens.PauseScreen);
    }

    public void GoBackToGame()
    {
        if(OnUnpause != null)
        {
            OnUnpause();
        }
        TransitionTo(Screens.GameScreen);
    }
    #endregion

    //Catch all transition command that activates the appropriate transition states
    private void TransitionTo(Screens screen)
    {
        if (Screens.CustomisationScreen == screen)
        {
            mTransState = TransitionState.Right;
            mTransPos = 1;
        } else if (Screens.PauseScreen == screen)
        {
            mTransState = TransitionState.Down;
            mTransPos = 1;
        } else if ((Screens)mCurrentScreen == Screens.CustomisationScreen)
        {
            mTransState = TransitionState.Left;
            mTransPos = 1;
        } else if ((Screens)mCurrentScreen == Screens.PauseScreen)
        {
            if(Screens.GameScreen==screen)
                mTransState = TransitionState.UpToGame;
            else
                mTransState = TransitionState.UpToTitle;
            mTransPos = 1;
        } else {
            if ((Screens)screen == Screens.TitleScreen)
            {
                transform.rotation = Quaternion.Euler(0, 20, 0);
            } else
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            mScreens[(int)mCurrentScreen].enabled = false;
        }
        mScreens[(int)screen].enabled = true;
        mPrevScreen = mCurrentScreen;
        mCurrentScreen = screen;
    }

    public void QuitGame()
    {
        SaveDataManager.SaveUserData();
        Application.Quit();
    }

    #region customisation button commands
    public void Custom1()
    {
        SaveDataManager.SetCustomisation(1, 0);
        if (OnPlayerChange != null)
        {
            OnPlayerChange();
        }
    }
    public void Custom2()
    {
        SaveDataManager.SetCustomisation(1, 1);
        if (OnPlayerChange != null)
        {
            OnPlayerChange();
        }
    }
    public void Custom3()
    {
        SaveDataManager.SetCustomisation(1, 2);
        if (OnPlayerChange != null)
        {
            OnPlayerChange();
        }
    }
    public void Custom4()
    {
        SaveDataManager.SetCustomisation(2, 0);
        if (OnPlayerChange != null)
        {
            OnPlayerChange();
        }
    }
    public void Custom5()
    {
        SaveDataManager.SetCustomisation(2, 1);
        if (OnPlayerChange != null)
        {
            OnPlayerChange();
        }
    }
    public void Custom6()
    {
        SaveDataManager.SetCustomisation(2, 2);
        if (OnPlayerChange != null)
        {
            OnPlayerChange();
        }
    }
    public void Custom7()
    {
        SaveDataManager.SetCustomisation(3, 0);
        if (OnPlayerChange != null)
        {
            OnPlayerChange();
        }
    }
    public void Custom8()
    {
        SaveDataManager.SetCustomisation(3, 1);
        if (OnPlayerChange != null)
        {
            OnPlayerChange();
        }
    }
    public void Custom9()
    {
        SaveDataManager.SetCustomisation(3, 2);
        if (OnPlayerChange != null)
        {
            OnPlayerChange();
        }
    }
    #endregion
}