﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomStartPosition : MonoBehaviour
{
    void Start()
    {
        float hw = Arena.Width * 0.5f;
        float hh = Arena.Height * 0.5f;
        float x = Random.Range(0, hw); //Spawn on right hand side of screen
        float y = Random.Range(-hh, hh);
        transform.position = new Vector3(x, y, 0);
    }
}