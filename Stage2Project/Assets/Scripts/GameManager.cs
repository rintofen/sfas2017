﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Whilst almost all events are defined in ScreenManager, we need one here just like OnTEndGame so that when enemies die they are subtracted from the enemy count.
    private static event ScreenManager.GameEvent OnEnemyKilled;

    //Keep track of what state the game is in.
    public enum State { Menu, Paused, Playing }
    //Used amongst all objects that need to know (player, bullets, enemies)
    public static State GameState;

    //Previously SpawnPrefabs, simply a list of enemy prefabs
    [SerializeField]
    private GameObject[] Enemies;

    //The number of seconds between enemy spawns (during a stage)
    [SerializeField]
    private float SpawnRate;
    //Number of enemies that must be spawned before they must all be cleared to progress to the next stage.
    [SerializeField]
    private int NumEnemiesPerStage;
    //The amount of time to wait before progressing onto the next stage regardless of how many enemies are left.
    [SerializeField]
    private int NumSecsPerStage;
    
    [SerializeField]
    private Player PlayerPrefab;
    //basic sprite prefab for slotting in player customisations
    [SerializeField]
    private GameObject CustomImagePrefab;

    //the "Arena". Was originally a cuboid that filled the screen but now it is simply used to store width and height of the screen in world space.
    [SerializeField]
    private Arena Arena;

    //Fill these in with the sprites for player customisation
    [SerializeField]
    private Sprite[] Hats;
    [SerializeField]
    private Sprite[] Robes;
    [SerializeField]
    private Sprite[] Hair;

    //mObjects contains all enemies, used to clear all the enemies when the game ends.
    private List<GameObject> mObjects;
    private Player mPlayer;
    private GameObject mHat;
    private GameObject mRobe;
    private GameObject mHair;

    //set of variables used for controlling the 'player' on menu screens
    private float mFloatTime;
    private float mLerpVar;
    private bool mLerping;
    //mMainMenu: true if the main menu is selected, false if it's the customisation menu - unused if on any other menu.
    private bool mMainMenu;

    //The score - in reality just how long you've lasted.
    public float Score;

    //mTicker is countdown until next enemy, mEnemies is how many enemies we've had so we can stop spawning at some point.
    private float mEnemyTicker;
    private float mEnemies;
    private float mStageTicker;
    private int mStage;
    private bool mStopSpawning;
    private int mNumEnemiesPerStageOnStart;

    void Awake()
    {
        //setup the player and their custom graphics
        mPlayer = Instantiate(PlayerPrefab);
        mPlayer.transform.parent = transform;

        mHat = Instantiate(CustomImagePrefab);
        mHat.transform.parent = mPlayer.transform;
        if (SaveDataManager.CurrentUserData.mHat < 2)
            mHat.GetComponent<SpriteRenderer>().sprite = Hats[SaveDataManager.CurrentUserData.mHat];
        mHat.transform.localPosition = new Vector3(0.026f, 0.18f, 0);

        mRobe = Instantiate(CustomImagePrefab);
        mRobe.transform.parent = mPlayer.transform;
        if (SaveDataManager.CurrentUserData.mRobe < 2)
            mRobe.GetComponent<SpriteRenderer>().sprite = Robes[SaveDataManager.CurrentUserData.mRobe];
        mRobe.GetComponent<SpriteRenderer>().sortingOrder++;
        mRobe.transform.localPosition = Vector3.zero;

        mHair = Instantiate(CustomImagePrefab);
        mHair.transform.parent = mPlayer.transform;
        if (SaveDataManager.CurrentUserData.mHair < 2)
            mHair.GetComponent<SpriteRenderer>().sprite = Hair[SaveDataManager.CurrentUserData.mHair];
        mHair.GetComponent<SpriteRenderer>().sortingOrder--;
        mHair.transform.localPosition = Vector3.zero;

        //setup events caused by button presses
        ScreenManager.OnNewGame += SM_OnNewGame;
        ScreenManager.OnEndGame += SM_OnEndGame;
        ScreenManager.OnMoveToCustomise += SM_OnMoveToCustomise;
        ScreenManager.OnMoveToMainMenu += SM_OnMoveToMainMenu;
        ScreenManager.OnPlayerChange += SM_RefreshPlayer;
        ScreenManager.OnPause += SM_OnPause;
        ScreenManager.OnUnpause += SM_OnUnpause;
        OnEnemyKilled += KillEnemy;
    }

    void Start()
    {
        //use Arena's calculations to check if player, enemies or bullets are out of bounds
        Arena.Calculate();
        
        GameState = State.Menu;
        //Initialises the player to start floating on the right hand side of the title menu
        mPlayer.transform.position = new Vector3(9.3f, 0, -0.5f);
        mPlayer.transform.localScale = new Vector3(30, 30, 1);
        mPlayer.transform.localRotation = Quaternion.Euler(0, 180, 0);

        Score = 0;
        mFloatTime = 0;
        mLerpVar = 1;
        mLerping = false;
        mMainMenu = true;
        mEnemyTicker = SpawnRate;
        mStageTicker = NumSecsPerStage;
        mStage = 0;
        mStopSpawning = false;
        mObjects = new List<GameObject>();
        mNumEnemiesPerStageOnStart = NumEnemiesPerStage;
    }

    void Update()
    {
        if (GameState == State.Playing)
        {
            Score += Time.deltaTime;
            mEnemyTicker -= Time.deltaTime;
            mStageTicker -= Time.deltaTime;
            if (mEnemyTicker < 0)
            {
                if (mEnemies < NumEnemiesPerStage && !mStopSpawning)
                {
                    GameObject tEnemy = Instantiate(Enemies[Random.Range(0, mStage+1)]);
                    tEnemy.SendMessage("SetPlayer", mPlayer);
                    tEnemy.transform.parent = transform;
                    mObjects.Add(tEnemy);
                    mEnemies++;
                }
                else
                {
                    mStopSpawning = true;
                    if (mEnemies == 0 || mStageTicker < 0)
                    {
                        //If we've run out of unique enemy types, just increase the number that spawn on a stage
                        if (mStage + 1 >= Enemies.Length)
                        {
                            NumEnemiesPerStage++;
                        }
                        else
                        {
                            mStage++;
                        }
                        mStopSpawning = false;
                        mStageTicker = NumSecsPerStage;
                    }
                }
                mEnemyTicker = SpawnRate;
            }
        }
        else if (GameState == State.Menu)
        {
            //Position the player (or interpolate if appropriate) on the left or right side depending on the menu. Better here than in Player as it leaves Player for actual gameplay.
            if (!mLerping)
            {
                if (mMainMenu)
                    mPlayer.transform.position = new Vector3(9.3f, Mathf.Sin(mFloatTime), -0.5f);
                else
                    mPlayer.transform.position = new Vector3(-9.3f, Mathf.Sin(mFloatTime), -0.5f);
            }
            else
            {
                Vector3 from;
                Vector3 to;
                if (mMainMenu)
                {
                    from = new Vector3(-9.3f, Mathf.Sin(mFloatTime), -0.5f);
                    to = new Vector3(9.3f, Mathf.Sin(mFloatTime), -0.5f);
                }
                else
                {
                    to = new Vector3(-9.3f, Mathf.Sin(mFloatTime), -0.5f);
                    from = new Vector3(9.3f, Mathf.Sin(mFloatTime), -0.5f);
                }
                if (mLerpVar > 0)
                {
                    mLerpVar -= Time.deltaTime * 2;
                    mPlayer.transform.position = Vector3.Lerp(from, to, 1.0f - Mathf.Pow(mLerpVar, 2));
                }
                else
                {
                    mPlayer.transform.position = to;
                    mLerping = false;
                }
            }
            mFloatTime += Time.deltaTime;
        }

        //Control the background's static variables
        MoveBG.mPos -= MoveBG.Speed * Time.deltaTime;
        if (MoveBG.mPos < -1.0f)
        {
            MoveBG.mPos = 0;
        }
    }

    //External function used by dead enemies, executing an event for the GameManager.
    public static void EnemyKillEvent()
    {
        if(OnEnemyKilled != null)
        {
            OnEnemyKilled();
        }
    }

    private void KillEnemy()
    {
        mEnemies--;
    }

    private void BeginNewGame()
    {        
        mPlayer.transform.rotation = Quaternion.identity;
        mPlayer.transform.localScale = new Vector3(8, 8, 1);
        mPlayer.transform.position = new Vector3(-10, 0, 0);
        Score = 0;
        mStage = 0;
        mEnemies = 0;
        mStageTicker = NumSecsPerStage;
        mEnemyTicker = SpawnRate;
        NumEnemiesPerStage = mNumEnemiesPerStageOnStart;
        GameState = State.Playing;
    }

    private void EndGame()
    {
        //Delete any remaining enemies
        for(int i = 0; i < mObjects.Count; i++)
        {
            if(mObjects[i] != null)
            {
                Destroy(mObjects[i]);
            }
        }
        mObjects.Clear();

        GameState = State.Menu;
        //Save high scores
        if (Score > SaveDataManager.CurrentUserData.mHighScore)
        {
            SaveDataManager.CurrentUserData.mHighScore = Score;
            SaveDataManager.SaveUserData();
        }
        mMainMenu = false;
    }

    private void SM_OnNewGame()
    {
        BeginNewGame();
    }

    private void SM_OnEndGame()
    {
        EndGame();
    }

    private void SM_OnPause()
    {
        GameState = State.Paused;
    }

    private void SM_OnUnpause()
    {
        GameState = State.Playing;
    }

    private void SM_OnMoveToCustomise()
    {
        mMainMenu = false;
        mLerping = true;
        mLerpVar = 1;
        mPlayer.transform.rotation = Quaternion.identity;
    }

    private void SM_OnMoveToMainMenu()
    {
        mMainMenu = true;
        mLerping = true;
        mLerpVar = 1;
        mPlayer.transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    //Reload player graphics from save data.
    private void SM_RefreshPlayer()
    {
        if (SaveDataManager.CurrentUserData.mHair < 2)
            mHair.GetComponent<SpriteRenderer>().sprite = Hair[SaveDataManager.CurrentUserData.mHair];
        else
            mHair.GetComponent<SpriteRenderer>().sprite = null;
        if (SaveDataManager.CurrentUserData.mRobe < 2)
            mRobe.GetComponent<SpriteRenderer>().sprite = Robes[SaveDataManager.CurrentUserData.mRobe];
        else
            mRobe.GetComponent<SpriteRenderer>().sprite = null;
        if (SaveDataManager.CurrentUserData.mHat < 2)
            mHat.GetComponent<SpriteRenderer>().sprite = Hats[SaveDataManager.CurrentUserData.mHat];
        else
            mHat.GetComponent<SpriteRenderer>().sprite = null;
    }
}
