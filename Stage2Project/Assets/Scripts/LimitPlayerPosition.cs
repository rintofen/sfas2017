﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//previously WrapPosition, repurposed for shmup player (prevents them falling off the screen)

public class LimitPlayerPosition : MonoBehaviour
{
    //How far from the boundaries will the player stop?
    private static float offset = 1;

	void Update ()
    {
        Vector3 position = transform.position;

        //Use the now defunct arena's measurements to check when off screen.
        if (position.x < (Arena.Width * -0.5f) + offset)
        {
            position.x = Arena.Width * -0.5f + offset;
        }
        else if (position.x > Arena.Width * 0.5f - offset)
        {
            position.x = Arena.Width * 0.5f - offset;
        }

        if (position.y < Arena.Height * -0.5f + offset)
        {
            position.y = Arena.Height * -0.5f + offset;
        }
        else if (position.y > Arena.Height * 0.5f - offset)
        {
            position.y = Arena.Height * 0.5f - offset;
        }

        transform.position = position;
    }
}
