﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour 
{
    //list of scenes to be loaded
	[SerializeField] private string [] Scenes;

    //only loads scenes between frames, uses this object
	private WaitForEndOfFrame mWaitForEndOfFrame;

    //lets others check on the state of loading
	public bool Loading { get; private set; }

	void Awake()
    {
        //Load user data if there is any, or just make a new set of data
        SaveDataManager.LoadUserData();
        mWaitForEndOfFrame = new WaitForEndOfFrame();
        Loading = true;
        //helps to run long asynchronous operations in a coroutine
        StartCoroutine(LoadScenes());
    }

	private IEnumerator LoadScenes()
	{
        //loop loading all listed scenes
		for( int count = 0; count < Scenes.Length; count++ )
		{
            //asynchronously start loading a scene
            AsyncOperation ao = SceneManager.LoadSceneAsync(Scenes[count], LoadSceneMode.Additive);
			if( ao != null )
			{
                //only check if load is done every frame
				while( !ao.isDone )
				{
					yield return mWaitForEndOfFrame;
				}
			}
		}

        //again wait until end of frame
		yield return mWaitForEndOfFrame;

        //finished loading
		Loading = false;
	}
}
