﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Speed at which they move with the arrow keys
    [SerializeField]
    private float Speed;

    //The fireball object to spawn when shooting
    [SerializeField]
    private Fireball fBallPrefab;

    //Amount of time in secs between consecutive shots
    [SerializeField]
    private float ShootCooldown;

    //Starting health of the player at the beginning of a game, defind in prefab.
    [SerializeField]
    private int StartHealth;

    //Used to keep track of how long until next shot.
    private float mCooldown;
    //Health during gameplay
    private int mHealth;

    void Awake()
    {
        ScreenManager.OnEndGame += SM_EndGame;
    }

    void Start()
    {
        mHealth = StartHealth;
    }

    void Update()
    {
        //Only allow player input if playing.
        if (GameManager.GameState == GameManager.State.Playing)
        {
            Vector3 direction = Vector3.zero;

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                direction = -Vector3.right;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                direction = Vector3.right;
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                direction += Vector3.up;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                direction += -Vector3.up;
            }

            direction.Normalize();
            transform.position += direction * Speed * Time.deltaTime;

            //Shooting code. Simply spawns 3 fireballs in a fixed relative position to the player.
            if (Input.GetKey(KeyCode.Z) && mCooldown < 0)
            {
                Fireball fTemp1 = Instantiate(fBallPrefab);
                fTemp1.transform.position = transform.position + new Vector3(1.9f, 0.7f, 0);
                fTemp1.transform.parent = transform.parent;

                Fireball fTemp2 = Instantiate(fBallPrefab);
                fTemp2.transform.position = transform.position + new Vector3(1.6f, 1.2f, 0);
                fTemp2.transform.parent = transform.parent;

                Fireball fTemp3 = Instantiate(fBallPrefab);
                fTemp3.transform.position = transform.position + new Vector3(1.6f, 0.2f, 0);
                fTemp3.transform.parent = transform.parent;

                mCooldown = ShootCooldown;
            }
            else
            {
                mCooldown -= Time.deltaTime;
            }

            //Let player health be represented by their transparency.
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, (float)mHealth / StartHealth);
        }
    }

    //Called by bullets to harm the player.
    public void Hurt(int damage)
    {
        mHealth -= damage;
        if(mHealth <= 0)
        {
            ScreenManager.ExtEndGame();
        }
    }

    //Cleans up the player at the end of a game.
    public void SM_EndGame()
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        transform.localScale = new Vector3(30, 30, 1);
        mHealth = StartHealth;
    }
}
