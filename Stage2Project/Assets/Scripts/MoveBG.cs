﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBG : MonoBehaviour {
    
    //To have a parallax effect, allow variable speeds. Maximum supported speed is 2.
    [SerializeField]
    private int Rate2orLess;

    //Common speed amongst all background elements. Initial value is slow speed
    public static float Speed = 0.2f;
    public static float SlowSpeed = Speed;
    public static float FastSpeed = Speed * 2;

    //Common tracked position amongst all background elements (otherwise they may get out of sync)
    public static float mPos = 0;
    
    //Used to keep track of whether the BGs are speeding up, slowing down, or constant in speed.
    enum SpeedState { Constant, Up, Down };
    private SpeedState mCurrentState;

    //Used to keep track of whether the camera should be looking at the trees or the earth.
    enum ViewState { Game, Pause, Up, Down }
    private ViewState mGameState;

    //Variables used for moving the BG between trees and earth.
    private float mLerpVar;
    private float mYPos;
    
	void Start () {
        ScreenManager.OnNewGame += SpeedUp;
        ScreenManager.OnEndGame += SlowDown;
        ScreenManager.OnPause += Paused;
        ScreenManager.OnUnpause += Resumed;
        mCurrentState = SpeedState.Constant;
        mGameState = ViewState.Game;
        mLerpVar = 1;
        mYPos = 0;
	}

    void Update()
    {
        //Speed up or slow down if appropriate.
        if (SpeedState.Up == mCurrentState)
        {
            if (Speed < FastSpeed)
                Speed += Time.deltaTime * (FastSpeed - SlowSpeed) / 2;
            else
                mCurrentState = SpeedState.Constant;
        }
        else if (SpeedState.Down == mCurrentState)
        {
            if (Speed > SlowSpeed)
                Speed -= Time.deltaTime * (FastSpeed - SlowSpeed) / 2;
            else
                mCurrentState = SpeedState.Constant;
        }

        //Shift from tree to earth or earth to tree if appropriate.
        if (ViewState.Down == mGameState)
        {
            BGLerp(0, 1.4f);
        } else if(ViewState.Up == mGameState)
        {
            BGLerp(1.4f, 0);
        }

        //Set position of the BG, if its rate is high then may need to offset it again.
        if (mPos * Rate2orLess * 2.5f < -2.5f)
            transform.localPosition = new Vector3(mPos * Rate2orLess * 2.5f + 2.5f, mYPos, 0);
        else
            transform.localPosition = new Vector3(mPos * Rate2orLess * 2.5f, mYPos, 0);
    }

    //A quadratic intepolation.
    private void BGLerp(float from, float to)
    {
        if (mLerpVar > 0)
        {
            mLerpVar -= Time.deltaTime * 2;
            mYPos = Mathf.Lerp(from, to, 1.0f - Mathf.Pow(mLerpVar, 2));
        }
        else
        {
            mYPos = to;
            mGameState = ViewState.Pause;
        }
    }

    //When game starts, speed the background up to make things more intense
    void SpeedUp()
    {
        mCurrentState = SpeedState.Up;
    }

    //When game ends, slow it down so as not to be distracting
    void SlowDown()
    {
        mCurrentState = SpeedState.Down;
    }

    //When we pause, we want to look through the earth and hide the game's objects from view
    void Paused()
    {
        mGameState = ViewState.Down;
        GetComponent<SpriteRenderer>().sortingOrder += 10;
        GetComponent<SpriteRenderer>().sortingLayerName = "Default";
        mLerpVar = 1;
    }

    //When we play, bring game's objects back into view.
    void Resumed()
    {
        mGameState = ViewState.Up;
        GetComponent<SpriteRenderer>().sortingOrder -= 10;
        GetComponent<SpriteRenderer>().sortingLayerName = "BG";
        mLerpVar = 1;
    }
}
