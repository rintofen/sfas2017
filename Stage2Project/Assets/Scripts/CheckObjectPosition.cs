﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//previously WrapPosition, repurposed for shmup objects that are supposed to disappear when they fall off screen e.g. enemies and bullets

public class CheckObjectPosition : MonoBehaviour
{
    //How far off the screen can the object go before being destroyed?
    private static float offset = 1;

	void Update ()
    {
        Vector3 position = transform.position;

        //Use the now defunct arena's measurements to check when off screen.
        if (position.x < Arena.Width * -0.5f - offset)
        {
            if(gameObject.tag=="Enemy")
                GameManager.EnemyKillEvent();
            DestroyObject(gameObject);
        }
        else if (position.x > Arena.Width * 0.5f + offset)
        {
            if (gameObject.tag == "Enemy")
                GameManager.EnemyKillEvent();
            DestroyObject(gameObject);
        }

        if (position.y < Arena.Height * -0.5f - offset)
        {
            if (gameObject.tag == "Enemy")
                GameManager.EnemyKillEvent();
            DestroyObject(gameObject);
        }
        else if (position.y > Arena.Height * 0.5f + offset)
        {
            if (gameObject.tag == "Enemy")
                GameManager.EnemyKillEvent();
            DestroyObject(gameObject);
        }
    }
}
