﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public static class SaveDataManager {
    //The user's data should be accessible by anyone.
    public static UserData CurrentUserData;

    public static void SaveUserData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/userdata.ws");
        bf.Serialize(file, CurrentUserData);
        file.Close();
    }

    public static void LoadUserData()
    {
        if (File.Exists(Application.persistentDataPath + "/userdata.ws"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/userdata.ws", FileMode.Open);
            CurrentUserData = (UserData)bf.Deserialize(file);
            file.Close();
        } else
        {
            CurrentUserData = new UserData();
        }
    }

    //Used by the customisation buttons.
    public static void SetCustomisation(int key, int value)
    {
        switch(key)
        {
            case 1:
                CurrentUserData.mHat = value;
                break;
            case 2:
                CurrentUserData.mRobe = value;
                break;
            case 3:
                CurrentUserData.mHair = value;
                break;
            default:
                break;
        }
    }
}
