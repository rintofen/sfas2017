﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextShowingScore : MonoBehaviour {
    
    //This class keeps trying to find the GameManager, until it does. Once it has it, it can safely set its text to the current score.
    private GameManager gm;
    
	void Start () {
        if(FindObjectOfType<GameManager>()!=null)
        gm = FindObjectOfType<GameManager>();
	}
	
	void Update () {
        if (gm == null)
        {
            if (FindObjectOfType<GameManager>() != null)
                gm = FindObjectOfType<GameManager>();
        }
        else
        {
            GetComponent<Text>().text = "Score: " + gm.Score.ToString("0.00");
        }
	}
}
