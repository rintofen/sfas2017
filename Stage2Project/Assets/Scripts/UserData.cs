﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//UserData simply stores the player's customisation choices and their high score.
[System.Serializable]
public class UserData {
    public int mHat { get; set; }
    public int mRobe { get; set; }
    public int mHair { get; set; }
    public float mHighScore { get; set; }

    public UserData(int pHat, int pRobe, int pHair, uint pHighScore)
    {
        mHat = pHat;
        mRobe = pRobe;
        mHair = pHair;
        mHighScore = pHighScore;
        SaveDataManager.CurrentUserData = this;
    }
    
    public UserData()
    {
        mHat = 0;
        mRobe = 0;
        mHair = 0;
        mHighScore = 0;
        SaveDataManager.CurrentUserData = this;
    }
}